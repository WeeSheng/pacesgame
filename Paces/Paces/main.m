//
//  main.m
//  Paces
//
//  Created by LEOW WEE SHENG on 3/10/13.
//  Copyright (c) 2013 LEOW WEE SHENG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
