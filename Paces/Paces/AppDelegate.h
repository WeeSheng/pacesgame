//
//  AppDelegate.h
//  Paces
//
//  Created by LEOW WEE SHENG on 3/10/13.
//  Copyright (c) 2013 LEOW WEE SHENG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
